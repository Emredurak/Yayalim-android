package com.example.emre.yayalim.Model;

import android.os.Parcel;
import android.os.Parcelable;


public class Kategori implements Parcelable {
    private int kategori_id;
    private String kategori_basligi;
    private String kategori_aciklamasi;
    private int kategori_gorseli;

    public Kategori (int kategori_id, int kategori_gorseli,String kategori_basligi, String kategori_aciklamasi){
        super();
        this.setKategori_id(kategori_id);
        this.setKategori_gorseli(kategori_gorseli);
        this.setKategori_basligi(kategori_basligi);
        this.setKategori_aciklamasi(kategori_aciklamasi);
    }

    protected Kategori(Parcel in) {
        kategori_id = in.readInt();
        kategori_gorseli = in.readInt();
        kategori_basligi = in.readString();
        kategori_aciklamasi = in.readString();
    }

    public static final Creator<Kategori> CREATOR = new Creator<Kategori>() {
        @Override
        public Kategori createFromParcel(Parcel in) {
            return new Kategori(in);
        }

        @Override
        public Kategori[] newArray(int size) {
            return new Kategori[size];
        }
    };

    public int getKategori_id() {
            return kategori_id;
        }

        public void setKategori_id(int kategori_id) {
            this.kategori_id = kategori_id;
        }

        public int getKategori_gorseli() {
            return kategori_gorseli;
        }

        public void setKategori_gorseli(int kategori_gorseli) {
            this.kategori_gorseli = kategori_gorseli;
        }

    public String getKategori_basligi() {
            return kategori_basligi;
        }

        public void setKategori_basligi(String kategori_basligi) {
            this.kategori_basligi = kategori_basligi;
        }

        public String getKategori_aciklamasi() {
            return kategori_aciklamasi;
        }

        public void setKategori_aciklamasi(String kategori_aciklamasi) {
            this.kategori_aciklamasi = kategori_aciklamasi;
        }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(kategori_id);
        dest.writeString(kategori_basligi);
        dest.writeString(kategori_aciklamasi);
        dest.writeInt(kategori_gorseli);
    }
}
