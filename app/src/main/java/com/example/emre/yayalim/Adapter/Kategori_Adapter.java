package com.example.emre.yayalim.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emre.yayalim.Model.Kategori;
import com.example.emre.yayalim.R;

import java.util.List;


public class Kategori_Adapter extends RecyclerView.Adapter<Kategori_Adapter.MyViewHolder> {

    private LayoutInflater mInflater;
    private List<Kategori> mKategoriListesi;

    public Kategori_Adapter(List<Kategori> kategoriler) {
        this.mKategoriListesi = kategoriler;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.from(parent.getContext())
                .inflate(R.layout.kategori_satir_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Kategori kategori = mKategoriListesi.get(position);
        holder.kategoribasligi.setText(kategori.getKategori_basligi());

        holder.kategorigorseli.setImageResource(kategori.getKategori_gorseli());

    }

    @Override
    public int getItemCount() {
        return mKategoriListesi.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView kategoribasligi;
        ImageView kategorigorseli;

        MyViewHolder(View view) {
            super(view);
            kategorigorseli = (ImageView) view.findViewById(R.id.kategori_gorseli);
            kategoribasligi = (TextView) view.findViewById(R.id.kategori_basligi);

        }

    }

}
