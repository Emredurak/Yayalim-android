package com.example.emre.yayalim.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emre.yayalim.Activities.Yorumlar_activity;
import com.example.emre.yayalim.Model.Yorum;
import com.example.emre.yayalim.R;

import java.util.List;


public class Yorum_Adapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private List<Yorum> mYorumList;

        public Yorum_Adapter(Activity activity, List<Yorum> yorumlar) {
            //XML'i alıp View'a çevirecek inflater'ı örnekleyelim
            mInflater = (LayoutInflater) activity.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            //gösterilecek listeyi de alalım
            mYorumList = yorumlar;
        }



    @Override
    public int getCount() {
        return mYorumList.size();
    }

    @Override
    public Object getItem(int position) {
        return mYorumList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.yorumlar_satir_layout, parent, false);
        }

        ImageView yorumcugorseli = (ImageView) convertView.findViewById(R.id.yorum_kullanici_gorseli);

        TextView yorumsahibi =
                (TextView) convertView.findViewById(R.id.yorum_sahibi);
        //        haberbasligi.setSelected(true); //  tek satıra sığmayan yazının kayması için gerekli...
        TextView yorumzamani =
                (TextView) convertView.findViewById(R.id.yorum_tarihi);
        TextView yorumicerigi =
                (TextView) convertView.findViewById(R.id.yorum_icerigi);
        TextView yorumsayisi =
                (TextView) convertView.findViewById(R.id.yorumlar_yorumsayisi);

        TextView begenmesayisi =
                (TextView) convertView.findViewById(R.id.yorumlar_begenmesayisi);
        TextView begenmemesayisi =
                (TextView) convertView.findViewById(R.id.yorumlar_begenmemesayisi);


        // on click listener ları burada tanımlayacağız..
        LinearLayout yorumlarLayout = (LinearLayout) convertView.findViewById(R.id.yorumlar_layout);
        yorumlarLayout.setTag(position);
        yorumlarLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(),Yorumlar_activity.class);
                view.getContext().startActivity(i);
            }
        });


        Yorum yorum = mYorumList.get(position);


        yorumsahibi.setText(yorum.getKullanici_adi());
        yorumzamani.setText(yorum.getYorum_tarihi());
        yorumicerigi.setText(yorum.getYorum_icerigi());
        begenmesayisi.setText(yorum.getBegenme_sayisi());
        begenmemesayisi.setText(yorum.getBegenmeme_sayisi());
        yorumsayisi.setText(yorum.getYorum_sayisi());
        yorumcugorseli.setImageResource(R.color.textColorSecondary);


        return convertView;
    }
}
