package com.example.emre.yayalim.Activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.emre.yayalim.Adapter.MultipleImageChoose_Adapter;
import com.example.emre.yayalim.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class MutipleImageChooseActivity extends AppCompatActivity {

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private StorageReference storageReference;
    private String currentUserID;


    private boolean gorselEklendi = false;
    private boolean baslikEklendi = false;
    private boolean icerikEklendi = false;

    private static final int RESULT_LOAD_IMAGE = 1;
    private ImageButton choose;
    private ImageView backArrow;
    private ImageView forwardArrow;
    private RecyclerView recyclerView;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;
    private AppCompatButton gonder;
    private EditText baslik;
    private EditText icerik;

    private List<Uri> fileNameList;
    private List<String> fileDoneList;
    private Boolean videoMu = false;
    private StorageReference mStorage;
    private MultipleImageChoose_Adapter multipleImageChoose_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutiple_image_choose);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserID = firebaseAuth.getCurrentUser().getUid();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        choose = findViewById(R.id.multipleImageChoose);
        backArrow = findViewById(R.id.back_arrow_choosen_recyclerview);
        forwardArrow = findViewById(R.id.forward_arrow_choosen_recyclerview);
        recyclerView = findViewById(R.id.uploadList);
        linearLayout = findViewById(R.id.imagesView);
        progressBar = findViewById(R.id.yayIcerikProgressBar);
        baslik = findViewById(R.id.yayIcerikBaslik);
        icerik = findViewById(R.id.yayIcerikIcerik);
        gonder = findViewById(R.id.yayGonder);

        mStorage = FirebaseStorage.getInstance().getReference();

        fileNameList = new ArrayList<>();
        fileDoneList = new ArrayList<>();
        multipleImageChoose_adapter = new MultipleImageChoose_Adapter(fileNameList , fileDoneList);

        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(multipleImageChoose_adapter);

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                //i.setType("image/*,video/*");
                i.setType("*/*");
                i.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*", "video/*"});
                i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i,"Fotoğraf Seç"),RESULT_LOAD_IMAGE);
            }
        });
        baslik.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                baslikEklendi = true;
                gonderKontrol();
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(!TextUtils.isEmpty(baslik.getText())) {
                    baslikEklendi = true;
                }else{
                    baslikEklendi = false;
                }
                gonderKontrol();
            }
        });
        icerik.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                icerikEklendi = true;
                gonderKontrol();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(icerik.getText())) {
                    icerikEklendi = true;
                }else{
                    icerikEklendi = false;
                }
                gonderKontrol();
            }
        });

        gonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gonder();
            }
        });
    }

/*
    private void gonder(){

        final String haberBaslik = baslik.getText().toString();
        final String haberIcerik = icerik.getText().toString();

        if(gorselEklendi && baslikEklendi && icerikEklendi){
            progressBar.setVisibility(View.VISIBLE);
            Long tsLong = System.currentTimeMillis();
            String ts = tsLong.toString();
            String randomName = md5(currentUserID+ts);

            Haber haber = new Haber(randomName, currentUserID ,haberBaslik, haberIcerik,0,0,0);
            haber.setHaber_gorseleri();
            StorageReference filePath;
            if(!videoMu) {
                filePath = storageReference.child("news_images").child(haberBaslik).child(randomName + ".jpg");
            }else{
                filePath = storageReference.child("news_images").child(haberBaslik).child(randomName + ".mp4");
            }
            filePath.putFile(postUrl).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()){
                        String downloadUrl = task.getResult().getDownloadUrl().toString();

                        Map<String,Object> newsMap = new HashMap<>();
                        newsMap.put("imageUrl",downloadUrl);
                        newsMap.put("title",haberBaslik);
                        newsMap.put("content",haberIcerik);
                        newsMap.put("userID",currentUserID);
                        newsMap.put("timestamp", FieldValue.serverTimestamp());

                        firebaseFirestore.collection("News").add(newsMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentReference> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(YayIcerik.this,"Gönderim Başarılı!",Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(YayIcerik.this,AnasayfaActivity.class);
                                    startActivity(i);
                                    finish();
                                }else{

                                }

                                progressBar.setVisibility(View.GONE);
                            }

                        });
                    }else{

                    }
                }
            });
        }else{
            Toast.makeText(YayIcerik.this,"Haber Detayları Boş Bırakılamaz",Toast.LENGTH_SHORT).show();

        }



    }
*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK){
            choose.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);

            if(data.getClipData() != null){
                int totalSelected = data.getClipData().getItemCount();
                gorselEklendi = true;
                gonderKontrol();

                for(int i = 0; i<totalSelected; i++){
                    Uri fileUri = data.getClipData().getItemAt(i).getUri();
                    String fileName = getFileName(fileUri);

                    if(fileUri != null) {
                        String mimeType = isVideo(fileUri);
                        if(mimeType.startsWith("image")) {
                            MultipleImageChoose_Adapter.isVideo = false;
                            videoMu = false;
                        }
                        else if(mimeType.startsWith("video")) {
                            MultipleImageChoose_Adapter.isVideo = true;
                            videoMu = true;
                        }
                    }

                    fileNameList.add(fileUri);
                    multipleImageChoose_adapter.notifyDataSetChanged();
                    StorageReference fileToUpload = mStorage.child("Images").child(fileName);
                }

                //Toast.makeText(MutipleImageChooseActivity.this,"Multiple files selected",Toast.LENGTH_SHORT).show();

            }else if(data.getData() != null){

                Toast.makeText(MutipleImageChooseActivity.this,"Single image selected",Toast.LENGTH_SHORT).show();

                Uri selectedMedia = data.getData();
                ContentResolver cr = this.getContentResolver();
                String mime = cr.getType(selectedMedia);
                if(mime.toLowerCase().contains("video")) {
                    MultipleImageChoose_Adapter.isVideo = true;
                    videoMu = true;
                    Toast.makeText(MutipleImageChooseActivity.this,"Single video selected",Toast.LENGTH_SHORT).show();
                } else if(mime.toLowerCase().contains("image")) {
                    MultipleImageChoose_Adapter.isVideo = false;
                    videoMu = false;
                    Toast.makeText(MutipleImageChooseActivity.this,"Single photo selected",Toast.LENGTH_SHORT).show();
                }

                fileNameList.add(selectedMedia);
                gorselEklendi = true;
                gonderKontrol();

            }
            if(fileNameList.size()==1){
                backArrow.setVisibility(View.INVISIBLE);
                forwardArrow.setVisibility(View.INVISIBLE);
            }
        }
    }


    public String getFileName(Uri uri){
        String result = null;

        if(uri.getScheme().equals("content")){
            Cursor cursor = getContentResolver().query(uri,null,null,null,null);
            try{
                if(cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }finally{
                cursor.close();
            }
        }
        if(result == null){
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if(cut != -1){
                result = result.substring(cut + 1);

            }
        }



        return result;
    }

    private String isVideo(Uri fileUri){
        String[] columns = { MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.MIME_TYPE };

        Cursor cursor = getContentResolver().query(fileUri, columns, null, null, null);
        cursor.moveToFirst();

        int pathColumnIndex     = cursor.getColumnIndex( columns[0] );
        int mimeTypeColumnIndex = cursor.getColumnIndex( columns[1] );

        String contentPath = cursor.getString(pathColumnIndex);
        String mimeType    = cursor.getString(mimeTypeColumnIndex);
        cursor.close();
        return mimeType;
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
    private void gonderKontrol(){
        if(gorselEklendi && baslikEklendi && icerikEklendi){
            gonder.setEnabled(true);
            gonder.setBackgroundColor(ContextCompat.getColor(MutipleImageChooseActivity.this,R.color.colorPrimary));
        }else{
            gonder.setEnabled(false);
            gonder.setBackgroundColor(ContextCompat.getColor(MutipleImageChooseActivity.this,R.color.buttonNotEnabled));
        }
    }

}
