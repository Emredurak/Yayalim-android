package com.example.emre.yayalim.Tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emre.yayalim.Adapter.Haber_Adapter;
import com.example.emre.yayalim.Adapter.OneCikan_Haber_Adapter;
import com.example.emre.yayalim.Model.Haber;
import com.example.emre.yayalim.R;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import java.util.ArrayList;
import java.util.List;

public class Tab_enson extends Fragment{


    RecyclerView recyclerView_onecikan;
    RecyclerView recyclerView_anasayfa;
    List<Haber> haberler_anasayfa = new ArrayList<>();
    List<Haber> haberler_onecikan = new ArrayList<>();

    OneCikan_Haber_Adapter mAdapter;
    Haber_Adapter adaptor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enson_layout, container, false);


        /*final ListView liste = (ListView) view.findViewById(R.id.liste_enson);
        adaptor=new Haber_Adapter(this, haberler_vertical);
        liste.setAdapter(adaptor);

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    int haber_id=haberler_vertical.get(position).getHaber_id();  *//*     buradan alınan id ile AnaHaber activity de
                                                                           veritabanından bu id li haberin tüm detayları çağırılacak
                                                                            Şimdilik seçilen Haber nesnesini komple gönderiyoruz.
                                                                        *//*
                    Haber haber = haberler_vertical.get(position);
                Intent i;
                i = new Intent(getActivity(), AnaHaber.class);
                i.putExtra("id", haber);
                // Starts TargetActivity
                startActivity(i);
            }
        });
*/
        recyclerView_onecikan = (RecyclerView) view.findViewById(R.id.recyclerview_onecikan);
        mAdapter = new OneCikan_Haber_Adapter(haberler_onecikan);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_onecikan.setLayoutManager(mLayoutManager);

        recyclerView_onecikan.setItemAnimator(new DefaultItemAnimator());
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView_onecikan);
        recyclerView_onecikan.setHasFixedSize(true);
        recyclerView_onecikan.setAdapter(mAdapter);

        onecikan_habergetir();

        recyclerView_anasayfa = (RecyclerView) view.findViewById(R.id.recyclerview_anasayfa);
        adaptor = new Haber_Adapter(haberler_anasayfa);
        RecyclerView.LayoutManager LayoutManager =
                new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView_anasayfa.setLayoutManager(LayoutManager);
        snapHelper = new GravitySnapHelper(Gravity.TOP);
        recyclerView_anasayfa.setItemAnimator(new DefaultItemAnimator());
        snapHelper.attachToRecyclerView(recyclerView_anasayfa);

        recyclerView_anasayfa.setAdapter(adaptor);

        anasayfa_habergetir();


        return view;
    }

    private void anasayfa_habergetir() {
        haberler_anasayfa.add(new Haber(11,"Kullanıcı_1","Uzun bir aranın ardından yeniden uygulama yapımı başladı","Metin 1","1.7k","1","999k"));
        haberler_anasayfa.add(new Haber(22,"Kullanıcı_2","Haber Başlık 2","Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin Zekiii Metiiiin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin" +"Bu ne dünya kardeşim"+
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                " 2Metin 2Metin 2Metin 2Metin 2Metin 2Metin 2" +
                "","2","2","2"));
        haberler_anasayfa.add(new Haber(33,"Kullanıcı_3","Haber Başlık 3","Metin 3","3","3","3"));
        haberler_anasayfa.add(new Haber(44,"Kullanıcı_4","Haber Başlık 4","Metin 4","4","4","4"));
        haberler_anasayfa.add(new Haber(55,"Kullanıcı_5","Haber Başlık 5","Metin 5","5","5","5"));
        haberler_anasayfa.add(new Haber(66,"Kullanıcı_6","Haber Başlık 6","Metin 6","6","6","6"));
        haberler_anasayfa.add(new Haber(77,"Kullanıcı_7","Haber Başlık 7","Metin 7","7","7","7"));

        adaptor.notifyDataSetChanged();
    }

    private void onecikan_habergetir() {
        haberler_onecikan.add(new Haber(11,"Kullanıcı_1","Haber Başlık 1 Deneme Haberi","Metin 1","1.7k","1","999k"));
        haberler_onecikan.add(new Haber(22,"Kullanıcı_2","Bu haber bir başka dostum","Metin 2","2","2","2"));
        haberler_onecikan.add(new Haber(33,"Kullanıcı_3","Haber Başlık 3","Metin 3","3","3","3"));
        haberler_onecikan.add(new Haber(44,"Kullanıcı_4","Haber Başlık 4","Metin 4","4","4","4"));
        haberler_onecikan.add(new Haber(55,"Kullanıcı_5","Haber Başlık 5","Metin 5","5","5","5"));
        haberler_onecikan.add(new Haber(66,"Kullanıcı_6","Haber Başlık 6","Metin 6","6","6","6"));
        haberler_onecikan.add(new Haber(77,"Kullanıcı_7","Haber Başlık 7","Metin 7","7","7","7"));

        mAdapter.notifyDataSetChanged();

    }
}