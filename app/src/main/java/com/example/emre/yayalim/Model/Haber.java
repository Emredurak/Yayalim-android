package com.example.emre.yayalim.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Haber implements Parcelable {
    private int haber_id;
    private String haber_basligi;
    private String haber_metni;
    private String yorum_sayisi;
    private String begenme_sayisi;
    private String begenmeme_sayisi;
    private String haber_sahibi;
    private List<String> haber_gorseleri;

    public Haber(int haber_id,String haber_sahibi, String haber_basligi, String haber_metni,
                 String yorum_sayisi, String begenme_sayisi,
                 String begenmeme_sayisi) {
        super();
        this.setHaber_id(haber_id);
        this.setHaber_sahibi(haber_sahibi);
        this.setHaber_basligi(haber_basligi);
        this.setHaber_metni(haber_metni);
        this.setYorum_sayisi(yorum_sayisi);
        this.setBegenme_sayisi(begenme_sayisi);
        this.setBegenmeme_sayisi(begenmeme_sayisi);

    }

    public Haber() {

    }

   /* @Override
    public String toString() {
        return isim;
    }*/

    protected Haber(Parcel in) {
        haber_id = in.readInt();
        haber_sahibi = in.readString();
        haber_basligi = in.readString();
        haber_metni = in.readString();
        yorum_sayisi = in.readString();
        begenme_sayisi = in.readString();
        begenmeme_sayisi = in.readString();
        haber_gorseleri = in.readArrayList(ClassLoader.getSystemClassLoader());
    }

    public static final Creator<Haber> CREATOR = new Creator<Haber>() {
        @Override
        public Haber createFromParcel(Parcel in) {
            return new Haber(in);
        }

        @Override
        public Haber[] newArray(int size) {
            return new Haber[size];
        }
    };


    public int getHaber_id() {
        return haber_id;
    }

    public void setHaber_id(int haber_id) {
        this.haber_id = haber_id;
    }

    public String getHaber_sahibi() {
        return haber_sahibi;
    }

    public void setHaber_sahibi(String haber_sahibi) {
        this.haber_sahibi = haber_sahibi;
    }

    public String getHaber_basligi() {
        return haber_basligi;
    }

    public void setHaber_basligi(String haber_basligi) {
        this.haber_basligi = haber_basligi;
    }

    public String getHaber_metni() {
        return haber_metni;
    }

    public void setHaber_metni(String haber_metni) {
        this.haber_metni = haber_metni;
    }

    public String getYorum_sayisi() {
        return yorum_sayisi;
    }

    public void setYorum_sayisi(String yorum_sayisi) {
        this.yorum_sayisi = yorum_sayisi;
    }

    public String getBegenme_sayisi() {
        return begenme_sayisi;
    }

    public void setBegenme_sayisi(String begenme_sayisi) {
        this.begenme_sayisi = begenme_sayisi;
    }

    public String getBegenmeme_sayisi() {
        return begenmeme_sayisi;
    }

    public void setBegenmeme_sayisi(String begenmeme_sayisi) {
        this.begenmeme_sayisi = begenmeme_sayisi;
    }
    public void setHaber_gorseleri(List<String> haber_gorseleri){
        this.haber_gorseleri = haber_gorseleri;
    }
    public List<String> getHaber_gorseleri(){
        return haber_gorseleri;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(haber_id);
        dest.writeString(haber_sahibi);
        dest.writeString(haber_basligi);
        dest.writeString(haber_metni);
        dest.writeString(yorum_sayisi);
        dest.writeString(begenme_sayisi);
        dest.writeString(begenmeme_sayisi);
        dest.writeList(haber_gorseleri);
    }
}