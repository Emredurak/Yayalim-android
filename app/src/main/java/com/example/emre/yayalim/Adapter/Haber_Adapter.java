package com.example.emre.yayalim.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emre.yayalim.Activities.AnaHaber;
import com.example.emre.yayalim.Activities.Kullanici_profili;
import com.example.emre.yayalim.Activities.Yorumlar_activity;
import com.example.emre.yayalim.Model.Haber;
import com.example.emre.yayalim.R;

import java.util.List;


public class Haber_Adapter extends RecyclerView.Adapter<Haber_Adapter.MyViewHolder> {

    private List<Haber> haberler;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView haberbasligi, habersahibi,yorumsayisi,begenmesayisi,begenmemesayisi;
        ImageView habergorseli,sepete_ekle;
        Boolean eklendimi = false ;
        LinearLayout yorum_layout;
        int pozisyon;
        Haber haber;
        public MyViewHolder(final View view) {
            super(view);
            habergorseli =
                    (ImageView) view.findViewById(R.id.haber_gorseli_anasayfa);
            haberbasligi =
                    (TextView) view.findViewById(R.id.haber_basligi_anasayfa);
            habersahibi =
                    (TextView) view.findViewById(R.id.haber_sahibi_anasayfa);
            yorumsayisi =
                    (TextView) view.findViewById(R.id.yorum_sayisi_anasayfa);
            begenmesayisi =
                    (TextView) view.findViewById(R.id.begenme_sayisi_anasayfa);
            begenmemesayisi =
                    (TextView) view.findViewById(R.id.begenmeme_sayisi_anasayfa);
            sepete_ekle = (ImageView) view.findViewById(R.id.sepete_ekle_anasayfa);
            yorum_layout = (LinearLayout) view.findViewById(R.id.yorumlar_layout);

            sepete_ekle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(eklendimi){
                        sepete_ekle.setImageResource(R.drawable.ic_sepete_ekle);
                        eklendimi = false;
                    }else {
                        sepete_ekle.setImageResource(R.drawable.ic_sepete_eklendi);
                        eklendimi = true;
                    }
                }
            });

            yorum_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(),Yorumlar_activity.class);
                    v.getContext().startActivity(i);
                }
            });

            habersahibi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), Kullanici_profili.class);
                    v.getContext().startActivity(i);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                //    Toast.makeText(view.getContext(),"Pos. "+pozisyon,Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(v.getContext(),AnaHaber.class);
                    i.putExtra("id", haber);
                    v.getContext().startActivity(i);
                }
            });

        }

    }



    public Haber_Adapter(List<Haber> haberler) {
        this.haberler = haberler;
    }

    @Override
    public Haber_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.satir_layout, parent, false);


        return new Haber_Adapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(Haber_Adapter.MyViewHolder holder, int position) {
        Haber haber = haberler.get(position);
        holder.haberbasligi.setText(haber.getHaber_basligi());
        holder.habersahibi.setText(haber.getHaber_sahibi());
        holder.yorumsayisi.setText(haber.getYorum_sayisi());
        holder.begenmesayisi.setText(haber.getBegenme_sayisi());
        holder.begenmemesayisi.setText(haber.getBegenmeme_sayisi());
        holder.habergorseli.setImageResource(R.color.textColorSecondary);
        holder.haber = haber;
        holder.pozisyon = position;
    }

    @Override
    public int getItemCount() {
        return haberler.size();
    }
}