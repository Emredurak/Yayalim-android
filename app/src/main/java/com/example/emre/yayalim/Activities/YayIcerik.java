package com.example.emre.yayalim.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.emre.yayalim.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class YayIcerik extends AppCompatActivity {

    private Toolbar yayIcerikToolbar;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private StorageReference storageReference;

    private ImageView photo;
    private EditText baslik;
    private EditText icerik;
    private VideoView video;
    private AppCompatButton gonder;
    private ProgressBar progressBar;

    private Uri postUrl = null;
    private String currentUserID;
    private boolean gorselEklendi = false;
    private boolean baslikEklendi = false;
    private boolean icerikEklendi = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yay_icerik);
//        yayIcerikToolbar = findViewById(R.id.YayIcerikToolbar);
//        setSupportActionBar(yayIcerikToolbar);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserID = firebaseAuth.getCurrentUser().getUid();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        photo = findViewById(R.id.yayIcerikFoto);
        video = findViewById(R.id.yayIcerikVideo);
        baslik = findViewById(R.id.yayIcerikBaslik);
        icerik = findViewById(R.id.yayIcerikIcerik);
        gonder = findViewById(R.id.yayGonder);
        progressBar = findViewById(R.id.yayIcerikProgressBar);

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseVideo();
            }
        });
        gonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gonder();
            }
        });
        baslik.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                baslikEklendi = true;
                gonderKontrol();
            }

            @Override
            public void afterTextChanged(Editable s) {
                gonderKontrol();
            }
        });
        icerik.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                icerikEklendi = true;
                gonderKontrol();
            }

            @Override
            public void afterTextChanged(Editable s) {
                gonderKontrol();
            }
        });
    }


    private void chooseImage(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512,512)
                .setAspectRatio(1,1)
                .start(YayIcerik.this);
        gorselEklendi = true;
        gonderKontrol();
    }

    private void chooseVideo(){

    }

    private void gonder(){

        final String haberBaslik = baslik.getText().toString();
        final String haberIcerik = icerik.getText().toString();

        if(!(TextUtils.isEmpty(haberBaslik) || TextUtils.isEmpty(haberIcerik) || postUrl == null)){
            progressBar.setVisibility(View.VISIBLE);
            Long tsLong = System.currentTimeMillis();
            String ts = tsLong.toString();

            String randomName = md5(currentUserID+ts);
            StorageReference filePath;
            if(photo.getVisibility() == View.VISIBLE) {
                filePath = storageReference.child("news_images").child(haberBaslik).child(randomName + ".jpg");
            }else{
                filePath = storageReference.child("news_images").child(haberBaslik).child(randomName + ".mp4");
            }
            filePath.putFile(postUrl).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()){
                        String downloadUrl = task.getResult().getDownloadUrl().toString();

                        Map<String,Object> newsMap = new HashMap<>();
                        newsMap.put("imageUrl",downloadUrl);
                        newsMap.put("title",haberBaslik);
                        newsMap.put("content",haberIcerik);
                        newsMap.put("userID",currentUserID);
                        newsMap.put("timestamp",FieldValue.serverTimestamp());

                        firebaseFirestore.collection("News").add(newsMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentReference> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(YayIcerik.this,"Gönderim Başarılı!",Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(YayIcerik.this,AnasayfaActivity.class);
                                    startActivity(i);
                                    finish();
                                }else{

                                }

                                progressBar.setVisibility(View.GONE);
                            }

                        });
                    }else{

                    }
                }
            });
        }else{
            Toast.makeText(YayIcerik.this,"Haber Detayları Boş Bırakılamaz",Toast.LENGTH_SHORT).show();

        }



    }

    private void gonderKontrol(){
        if(gorselEklendi && baslikEklendi && icerikEklendi){
            gonder.setEnabled(true);
            gonder.setBackgroundColor(ContextCompat.getColor(YayIcerik.this,R.color.colorPrimary));
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                postUrl = result.getUri();
                if(video.getVisibility() != View.VISIBLE){
                    photo.setImageURI(postUrl);
                }else{
                    video.setVideoURI(postUrl);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
    public static String md5(String s)
    {
        MessageDigest digest;
        try
        {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")),0,s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return "";
    }
}
