package com.example.emre.yayalim.Activities;


import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.emre.yayalim.Adapter.OneCikan_Haber_Adapter;
import com.example.emre.yayalim.Adapter.ViewPagerAdapter;
import com.example.emre.yayalim.Model.Model_Search;
import com.example.emre.yayalim.R;
import com.example.emre.yayalim.Tabs.Ekle;
import com.example.emre.yayalim.Tabs.Tab_enson;
import com.example.emre.yayalim.Tabs.Tab_kategoriler;
import com.example.emre.yayalim.YAY;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.miguelcatalan.materialsearchview.MaterialSearchView;


public class AnasayfaActivity extends AppCompatActivity implements ActionBar.TabListener{

    Toolbar toolbar;
    MaterialSearchView searchView;
    Model_Search model_search;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private OneCikan_Haber_Adapter mAdapter;
    private RecyclerView recyclerView;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private boolean girisYapildiMi = false;
    private boolean facebookGirisi = false;
    private boolean epostaGirisi = false;
    private String name="";

    /*private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_harita:
                 //   mTextMessage.setText(R.string.title_Harita);
                    return true;
                case R.id.navigation_yay:
                 //   mTextMessage.setText(R.string.title_Yay);
                    return true;
                case R.id.navigation_galeri:
                 //   mTextMessage.setText(R.string.title_Galeri);
                    return true;
            }
            return false;
        }

    };*/

    @Override
    protected void onResume() {
        super.onResume();
        viewPager.setCurrentItem(1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        model_search = new Model_Search();
        model_search.setOnLoadSearchListener(new Model_Search.OnLoadSearchResultListener() {
            @Override
            public void onLoad(String[] sonuc) {

            }

            @Override
            public void onError() {

            }

            @Override
            public void onLocalLoad(String[] sonuc) {
                searchView.setSuggestions(sonuc);
            }
        });

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        search();

        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null){
            epostaGirisi = false;
        }else{
            epostaGirisi = true;
            name = auth.getCurrentUser().getDisplayName();
        }


         if(Profile.getCurrentProfile() == null){
             facebookGirisi = false;
         }else{
             facebookGirisi = true;
             name = Profile.getCurrentProfile().getFirstName();
         }



        //Bir AuthStateListener dinleyicisi oluşturuyoruz ve bu dinleyici onAuthStateChanged bölümünü çalıştırır.
        // Buradaki getCurrentUser metodu ile kullanıcı verilerine ulaşabilmekteyiz.
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                // Eğer geçerli bir kullanıcı oturumu yoksa LoginActivity e geçilir.
                // Oturum kapatma işlemi yapıldığında bu sayede LoginActivity e geçilir.
                if (user == null) {
                    epostaGirisi = false;
                }else{
                    epostaGirisi = true;
                    name = auth.getCurrentUser().getDisplayName();
                }
            }
        };

        girisYapildiMi = facebookGirisi || epostaGirisi;

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_yay);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_kategori);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.Default_Icon_Color_Primary);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.Default_Icon_Color_secondary);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }

        });

        /*habergetir();*/

        /*FloatingActionButton myFab = (FloatingActionButton)  findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Kamera_ac();
            }
        });
        Log.d("mesaj", "rapor");
*/
    }


    private void search() {
        searchView = findViewById(R.id.searchview);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Aranan kelime arananlar listesine eklenecek..
                // Burada arama gerçekleştirildikten sonra sonuçların yer aldığı yeni bir intent e yönlendirilecek..

                Intent i = new Intent (getApplicationContext(),Arama_Sonucu.class);
                i.putExtra("query",query);
                startActivity(i);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //model_search.querySearch(newText);
                return false;
            }
        });
    }


    private void Kamera_ac() {
        Toast.makeText(getApplicationContext(),"Kameranın açılması için kamaera iconu tıklandı",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AnasayfaActivity.this,YAY.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.anasayfa_toolbar_menu,menu);

        MenuItem item_search = menu.findItem(R.id.search);
        searchView.setMenuItem(item_search);

        if(girisYapildiMi){
            MenuItem menuItem=menu.findItem(R.id.item_giris);
            menuItem.setVisible(false);
            MenuItem menuItem1 = menu.findItem(R.id.item_cikis);
            menuItem1.setVisible(true);
            MenuItem menuItem2=menu.findItem(R.id.item_profil);
            menuItem2.setTitle("Profil");
            menuItem2.setVisible(true);
        } else{
            MenuItem menuItem=menu.findItem(R.id.item_giris);
            menuItem.setVisible(true);
            MenuItem menuItem2=menu.findItem(R.id.item_profil);
            menuItem2.setVisible(false);
            MenuItem menuItem1 = menu.findItem(R.id.item_cikis);
            menuItem1.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search) {
            searchView.showSearch(true);


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else
        {
            super.onBackPressed();
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Ekle(),null);
        adapter.addFragment(new Tab_enson(), null);
        adapter.addFragment(new Tab_kategoriler(), null);

        viewPager.setAdapter(adapter);


    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        int tabIconColor = ContextCompat.getColor(this, R.color.textColorPrimary);
        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        int tabIconColor = ContextCompat.getColor(this, R.color.textColorSecondary);
        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }






    public void home_click(MenuItem item){
        Toast.makeText(getApplicationContext(),"Ana sayfa iconu tıklandı",Toast.LENGTH_SHORT).show();
    }

    public void yay_click(MenuItem item) {
        Toast.makeText(getApplicationContext(),"Kameranın açılması için kamaera iconu tıklandı",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AnasayfaActivity.this,YAY.class);
        startActivity(intent);
    }

    public void galeri_click(MenuItem item) {
        Toast.makeText(getApplicationContext(),"Galerien seçim ekle iconu tıklandı",Toast.LENGTH_SHORT).show();
    }

    public void Giris_yap_Click(MenuItem item) {
        FirebaseUser user = auth.getCurrentUser();
            if(user==null) {
                Intent intent = new Intent(AnasayfaActivity.this, LoginActivity.class);
                startActivity(intent);
        }

    }

    public void cikisi_onayla() {
        //FirebaseAuth.getInstance().signOut ile oturumu kapatabilmekteyiz.
        auth.signOut();
        epostaGirisi = false;

        LoginManager.getInstance().logOut();
        facebookGirisi = false;

        Toast.makeText(getApplicationContext(),"Çıkış yapıldı",Toast.LENGTH_SHORT).show();

        AnasayfaActivity.this.recreate();
        /*Intent i = new Intent(AnasayfaActivity.this,AnasayfaActivity.class);
        startActivity(i);*/
    }
    public void cikis_yap(MenuItem item){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                AnasayfaActivity.this);

        // set title
        alertDialogBuilder.setTitle("Çıkış Yap");

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.cikis_onay_msg)
                .setCancelable(false)
                .setPositiveButton(R.string.evet,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        cikisi_onayla();
                    }
                })
                .setNegativeButton(R.string.hayır,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void profilimeGit(MenuItem item) {
        Intent i = new Intent(AnasayfaActivity.this, MyProfileActivity.class);
        startActivity(i);
    }
    public void ekle(MenuItem item){
        Intent intent = new Intent(AnasayfaActivity.this,MutipleImageChooseActivity.class);
        startActivity(intent);
    }
}
