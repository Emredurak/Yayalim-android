package com.example.emre.yayalim.Model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Yorum implements Parcelable{
    private int yorum_id;
    private String begenme_sayisi;
    private String begenmeme_sayisi;
    private String yorum_sayisi;
    private String yorum_tarihi;
    private String kullanici_adi;
    private Bitmap kullanici_resmi;
    private String yorum_icerigi;

    protected Yorum(Parcel in) {
        yorum_id = in.readInt();
        begenme_sayisi = in.readString();
        begenmeme_sayisi = in.readString();
        yorum_sayisi = in.readString();
        yorum_tarihi = in.readString();
        kullanici_adi = in.readString();
        kullanici_resmi = in.readParcelable(Bitmap.class.getClassLoader());
        yorum_icerigi = in.readString();
    }

    public static final Creator<Yorum> CREATOR = new Creator<Yorum>() {
        @Override
        public Yorum createFromParcel(Parcel in) {
            return new Yorum(in);
        }

        @Override
        public Yorum[] newArray(int size) {
            return new Yorum[size];
        }
    };

    public int getYorum_id() {
        return yorum_id;
    }

    public void setYorum_id(int yorum_id) {
        this.yorum_id = yorum_id;
    }

    public String getBegenme_sayisi() {
        return begenme_sayisi;
    }

    public void setBegenme_sayisi(String begenme_sayisi) {
        this.begenme_sayisi = begenme_sayisi;
    }

    public String getBegenmeme_sayisi() {
        return begenmeme_sayisi;
    }

    public void setBegenmeme_sayisi(String begenmeme_sayisi) {
        this.begenmeme_sayisi = begenmeme_sayisi;
    }

    public String getYorum_sayisi() {
        return yorum_sayisi;
    }

    public void setYorum_sayisi(String yorum_sayisi) {
        this.yorum_sayisi = yorum_sayisi;
    }

    public String getYorum_tarihi() {
        return yorum_tarihi;
    }

    public void setYorum_tarihi(String yorum_tarihi) {
        this.yorum_tarihi = yorum_tarihi;
    }

    public String getKullanici_adi() {
        return kullanici_adi;
    }

    public void setKullanici_adi(String kullanici_adi) {
        this.kullanici_adi = kullanici_adi;
    }

    public Bitmap getKullanici_resmi() {
        return kullanici_resmi;
    }

    public void setKullanici_resmi(Bitmap kullanici_resmi) {
        this.kullanici_resmi = kullanici_resmi;
    }

    public String getYorum_icerigi() {
        return yorum_icerigi;
    }

    public void setYorum_icerigi(String yorum_icerigi) {
        this.yorum_icerigi = yorum_icerigi;
    }

    public Yorum(int yorum_id, String kullanici_adi, String yorum_tarihi, String yorum_icerigi, String yorum_sayisi, String begenme_sayisi, String begenmeme_sayisi) {
        this.yorum_id = yorum_id;
        this.begenme_sayisi = begenme_sayisi;
        this.begenmeme_sayisi = begenmeme_sayisi;
        this.yorum_sayisi = yorum_sayisi;
        this.yorum_tarihi = yorum_tarihi;
        this.kullanici_adi = kullanici_adi;
        this.yorum_icerigi = yorum_icerigi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(yorum_id);
        dest.writeString(begenme_sayisi);
        dest.writeString(begenmeme_sayisi);
        dest.writeString(yorum_sayisi);
        dest.writeString(yorum_tarihi);
        dest.writeString(kullanici_adi);
        dest.writeParcelable(kullanici_resmi, flags);
        dest.writeString(yorum_icerigi);
    }
}
