package com.example.emre.yayalim.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emre.yayalim.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class KayitActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    String TAG = "Kayit Tag";

    private EditText email;
    private EditText parola;
    private EditText parola_tekrar;
    private EditText isim;
    private TextView giris_yap_button;
    private AppCompatButton üye_ol_button;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kayit);

        mAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.kayit_eposta);
        parola = findViewById(R.id.kayit_parola);
        parola_tekrar = findViewById(R.id.kayit_parola_tekrar);
        giris_yap_button = findViewById(R.id.kayıt_giris_yap_button);
        üye_ol_button = findViewById(R.id.kayıt_uye_ol_button);
        isim = findViewById(R.id.kayit_isim);
        progressBar = findViewById(R.id.kayit_progressBar);

        giris_yap_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giris_yap();
            }
        });

        üye_ol_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eposta = email.getText().toString().trim();
                String şifre = parola.getText().toString().trim();
                String şifre_tekrar = parola_tekrar.getText().toString().trim();
                String name = isim.getText().toString().trim();


                if(TextUtils.isEmpty(name)){
                    Toast.makeText(getApplicationContext(),"Lütfen adınızı giriniz",Toast.LENGTH_SHORT).show();
                }else
                if(TextUtils.isEmpty(eposta)){
                    Toast.makeText(getApplicationContext(),"Lütfen emailinizi giriniz",Toast.LENGTH_SHORT).show();
                }else
                if(TextUtils.isEmpty(şifre)){
                    Toast.makeText(getApplicationContext(),"Lütfen parolanızı giriniz",Toast.LENGTH_SHORT).show();
                }else
                if(!TextUtils.equals(şifre,şifre_tekrar)){
                    Toast.makeText(getApplicationContext(),"Parolalar uyuşmuyor",Toast.LENGTH_SHORT).show();
                }else
                if (parola.length()<6){
                    Toast.makeText(getApplicationContext(),"Parola en az 6 haneli olmalıdır",Toast.LENGTH_SHORT).show();
                }
                else {
                    progressBar.setVisibility(View.VISIBLE);
                    createUser(email.getText().toString(), parola.getText().toString());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    void updateUI(FirebaseUser currentUser){
        if(currentUser!=null) {
            Intent i = new Intent(KayitActivity.this, AnasayfaActivity.class);
            startActivity(i);
        }
    }
    void createUser(String email, String password){


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG,"createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(isim.getText().toString())
                                    .build();
                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "User profile updated.");
                                            }
                                        }
                                    });
                            progressBar.setVisibility(View.GONE);
                            updateUI(mAuth.getCurrentUser());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(KayitActivity.this, "Üzgünüz! Bu e-posta adresi zaten kullanımda.",
                                    Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                            updateUI(null);
                        }

                        // ...
                    }
                });

    }

    void Giris_Yap(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(KayitActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    void giris_yap(){
        Intent i = new Intent(KayitActivity.this, LoginActivity.class);
        startActivity(i);
    }
}
