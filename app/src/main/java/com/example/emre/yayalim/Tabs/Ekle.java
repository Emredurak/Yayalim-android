package com.example.emre.yayalim.Tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.emre.yayalim.Activities.MutipleImageChooseActivity;
import com.example.emre.yayalim.Activities.YayIcerik;
import com.example.emre.yayalim.R;

public class Ekle extends Fragment{

    public Ekle() {
    }

    private Button gonder;
    private Button multiChoose;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.populer_layout, container, false);
        gonder = view.findViewById(R.id.gonderiEkle);
        multiChoose = view.findViewById(R.id.multipleIntent);
        gonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), YayIcerik.class);
                startActivity(i);
            }
        });

        multiChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MutipleImageChooseActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}