package com.example.emre.yayalim.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emre.yayalim.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    String TAG = "LoginActivity";

    private AppCompatEditText email;
    private TextInputEditText parola;
    private AppCompatButton giris_yap_button;
    private TextView üye_ol_button;
    private TextView parola_sifirla_button;
    private ProgressBar progressBar;
    private AppCompatImageButton facebook_button;
    private AppCompatImageButton twitter_button;
    private AppCompatImageButton google_button;
    private CallbackManager callbackManager;
    private TwitterAuthClient mTwitterAuthClient;
    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;

    private int request = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));

        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();

        Twitter.initialize(twitterConfig);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mTwitterAuthClient = new TwitterAuthClient();
        //FirebaseAuth sınıfının referans olduğu nesneleri kullanmak için getInstance methodunu kullanıyoruz.
        mAuth = FirebaseAuth.getInstance();
        Twitter.initialize(this);

        email = findViewById(R.id.giris_eposta);
        parola = findViewById(R.id.giris_parola);
        giris_yap_button = findViewById(R.id.giris_yap_button);
        üye_ol_button = findViewById(R.id.uye_ol_button);
        parola_sifirla_button = findViewById(R.id.Parolamı_unuttum_button);
        progressBar = findViewById(R.id.giris_progressBar);
        facebook_button = findViewById(R.id.facebook_login_button);
        twitter_button = findViewById(R.id.twitter_login_button);
        google_button = findViewById(R.id.google_login_button);

        facebook_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                login_withFB();
            }
        });

        twitter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                login_withTW();
            }
        });

        google_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                login_withGoogle();
            }
        });

        giris_yap_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                String eposta = email.getText().toString().trim();
                String sifre = parola.getText().toString().trim();
                email.setText("");
                parola.setText("");
                Giris_Yap(eposta, sifre);

            }
        });

        üye_ol_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                üye_ol();
            }
        });

        parola_sifirla_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parola_sifirla();
            }
        });
    }

    void updateUI(FirebaseUser currentUser){
        if(currentUser!=null) {
            Intent i = new Intent(LoginActivity.this, AnasayfaActivity.class);
            startActivity(i);
            finish();
        }
    }

    void Giris_Yap(String email, String password){
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            Toast.makeText(LoginActivity.this, "Lütfen e-posta ve parolanızı girin.",
                    Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }else {

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                progressBar.setVisibility(View.GONE);
                                updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Giriş başarısız oldu.",
                                        Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                updateUI(null);
                            }

                            // ...
                        }
                    });
        }
    }

    void üye_ol(){
        Intent i = new Intent(LoginActivity.this, KayitActivity.class);
        startActivity(i);
    }
    void parola_sifirla() {
        String mail = email.getText().toString().trim();

        if (TextUtils.isEmpty(mail)) {
            Toast.makeText(getApplication(), "Lütfen e-posta adresinizi giriniz", Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.sendPasswordResetEmail(mail)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Yeni parola için gerekli bağlantı, adresinize gönderildi!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "Mail gönderme hatası!", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }


                    }
                });
    }
    void login_withFB(){
        request = 1;
        callbackManager = CallbackManager.Factory.create();
        //Toast.makeText(LoginActivity.this, "Facebook Login", Toast.LENGTH_SHORT).show();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile", "user_posts"));
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(callbackManager,

                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(LoginActivity.this, "Giriş Başarılı ", Toast.LENGTH_SHORT).show();
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(LoginActivity.this, "Facebook bağlantısında hata oluştu", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }
                );
    }

    private void handleFacebookAccessToken(AccessToken token){
        Log.d(TAG,"handleFacebookAccessToken: "+token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            progressBar.setVisibility(View.GONE);
                            updateUI(user);
                        }else {
                            Log.w(TAG,"signInWithCredential:failure",task.getException());
                            Toast.makeText(LoginActivity.this, "Facebook failed", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                });

    }

    void login_withTW(){
        request = 2;
        mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                // Success
                Toast.makeText(LoginActivity.this, "Giriş Başarılı ", Toast.LENGTH_SHORT).show();
                handleTwitterSession(result.data);

            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, "Twitter ile giriş başarısız", Toast.LENGTH_SHORT).show();

            }
        });
        progressBar.setVisibility(View.GONE);
    }
    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);
            AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

                    mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithCredential:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Twitter bağlantısı başarısız oldu.",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(null);
                            }
                        }
                });
    }

    void login_withGoogle(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Google yetkilendirmesi başarısız oldu.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (request == 1) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (request == 2) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
        request = 0;
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }
}
