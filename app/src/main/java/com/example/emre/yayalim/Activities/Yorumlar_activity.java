package com.example.emre.yayalim.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.example.emre.yayalim.Adapter.Yorum_Adapter;
import com.example.emre.yayalim.Model.Yorum;
import com.example.emre.yayalim.R;

import java.util.ArrayList;
import java.util.List;


public class Yorumlar_activity extends AppCompatActivity {
    Toolbar toolbar;
    List<Yorum> yorumlar = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yorumlar_layout);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //setSupportActionBar(toolbar);

        yorumlari_yukle();

    }

    private void yorumlari_yukle() {
        yorumlar.add(new Yorum(1, "YAYALIM", "2 gün önce","Kesin bilgi herkese yayalım","1","1","1"));
        yorumlar.add(new Yorum(2, "Emre", "15.07.1994","Bu ne biçim haber!..","2","2","2"));
        yorumlar.add(new Yorum(3, "Kullanıcı 3", "3 saat önce","Kesinlikle katılıyorum","3","3","3"));
        yorumlar.add(new Yorum(4, "ters dönen kaplumbağa", "4 ay önce","Keşke olmasa böyle şeyler","4","4","4"));
        yorumlar.add(new Yorum(5, "Yayalim", "2 dakika önce","Sanane olum","5","5","5"));
        yorumlar.add(new Yorum(6, "Kullanıcı 6", "6 dakika önce","Yay geç abi gerisine karışma","6","6","6"));


        final ListView liste = findViewById(R.id.liste_yorumlar);
        Yorum_Adapter adaptor = new Yorum_Adapter(this, yorumlar);
        liste.setAdapter(adaptor);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_normal,menu);

        return true;
    }
}