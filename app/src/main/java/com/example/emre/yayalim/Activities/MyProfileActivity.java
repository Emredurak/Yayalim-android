package com.example.emre.yayalim.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.emre.yayalim.R;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends AppCompatActivity {
    private CircleImageView profil_image;
    private TextInputEditText profile_name;
    private TextInputEditText Email;
    private TextInputEditText password;
    private TextInputEditText password_new;
    private TextInputEditText password_new_again;
    private AppCompatButton kaydet_button;
    private AppCompatButton iptal_button;
    private AppCompatImageView edit_name;
    private AppCompatImageView edit_email;
    private AppCompatImageView edit_password;
    private ProgressBar progressBar;


    private String userID;
    private String userName;
    private String eposta;
    private boolean emailChanged = false;
    private boolean NameChanged = false;
    private boolean passwordChanged = false;


    private int requestCode = 0;
    private Uri mainImageURI = null;
    private Uri default_uri = null;
    private Uri download_uri;
    private FirebaseAuth firebaseAuth;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;

    private String accounttype = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_layout);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();

        for (UserInfo userInfo : user.getProviderData()) {
            accounttype = userInfo.getProviderId();
        }

        profil_image = findViewById(R.id.profil_photo);
        profile_name = findViewById(R.id.myProfileName);
        Email = findViewById(R.id.myEmail);
        password = findViewById(R.id.myPasswordOld);
        password_new = findViewById(R.id.myPasswordNew);
        password_new_again = findViewById(R.id.myPassword_Again);
        progressBar = findViewById(R.id.myProgressBar);
        edit_name = findViewById(R.id.edit_isim);
        edit_email = findViewById(R.id.edit_eposta);
        edit_password = findViewById(R.id.edit_parola);
        kaydet_button = findViewById(R.id.kaydetMyProfile);
        iptal_button = findViewById(R.id.iptalMyProfile);

        if(accounttype.equalsIgnoreCase("Twitter.com") ||
           accounttype.equalsIgnoreCase("facebook.com") ||
           accounttype.equalsIgnoreCase("google.com")){
            edit_email.setVisibility(View.GONE);
            edit_name.setVisibility(View.GONE);
            edit_password.setVisibility(View.GONE);
        }

        getUserInformation(user);

        profile_name.setText(userName);
        Email.setText(eposta);

        profil_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ppClick();
            }
        });
        edit_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editName();
            }
        });
        edit_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEmail();
            }
        });
        edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPassword();
            }
        });
        kaydet_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kaydet_button.setEnabled(false);
                kaydet_button.setBackgroundColor(ContextCompat.getColor(MyProfileActivity.this,R.color.buttonNotEnabled));
                progressBar.setVisibility(View.VISIBLE);
                degisiklikleri_kaydet();
               // progressBar.setVisibility(View.GONE);
            }
        });
        iptal_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void getProfilePhoto (){
        progressBar.setVisibility(View.VISIBLE);
        firebaseFirestore.collection("Users").document(userID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){

                    if(task.getResult().exists()){
                        String name = task.getResult().getString("name");
                        String image = task.getResult().getString("image");


                        Glide.with(MyProfileActivity.this)
                                .load(image)
                                .into(profil_image);

                    }

                }else{
                    String error = task.getException().getMessage();
                    Toast.makeText(MyProfileActivity.this  ,"Hata! FireStore Profil resmini alamadı.",Toast.LENGTH_SHORT).show();

                }
            }
        });
        progressBar.setVisibility(View.GONE);
    }
    void getProfilePhoto2 (){
        progressBar.setVisibility(View.VISIBLE);
        final StorageReference reference = storageReference.child("Profile_Photos").child(userID+".jpg");
        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri downloadUri) {
                Glide.with(MyProfileActivity.this)
                        .using(new FirebaseImageLoader())
                        .load(reference)
                        .into(profil_image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Toast.makeText(MyProfileActivity.this,"Hata oluştu",Toast.LENGTH_SHORT).show();
            }
        });
        progressBar.setVisibility(View.GONE);
        profil_image.setVisibility(View.VISIBLE);
    }

    void ppClick(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MyProfileActivity.this,"İzin reddedildi",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(MyProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},requestCode);
            }else{
                resimSec();
                kaydet_button.setEnabled(true);
                kaydet_button.setBackgroundResource(R.color.colorPrimary);
            }
        } else{
            resimSec();
        }
    }
    void editName(){
        profile_name.setEnabled(true);
        NameChanged = true;
        kaydet_button.setEnabled(true);
        kaydet_button.setBackgroundResource(R.color.colorPrimary);
    }
    void editEmail(){
        Email.setEnabled(true);
        emailChanged = true;
        kaydet_button.setEnabled(true);
        kaydet_button.setBackgroundResource(R.color.colorPrimary);
    }
    void editPassword(){
        password.setEnabled(true);
        passwordChanged = true;
        password.setHint(R.string.ParolaEski);
        password_new.setVisibility(View.VISIBLE);
        password_new.setHint(R.string.ParolaYeni);
        password_new_again.setVisibility(View.VISIBLE);
        kaydet_button.setEnabled(true);
        kaydet_button.setBackgroundResource(R.color.colorPrimary);

    }
    void getUserInformation(FirebaseUser user){
        userID = user.getUid();
        userName = user.getDisplayName();
        eposta = user.getEmail();
        getProfilePhoto();
    }
    void degisiklikleri_kaydet(){
        String newUserName = profile_name.getText().toString();
        String email = Email.getText().toString();
        String oldPassword = password.getText().toString();
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        if(TextUtils.isEmpty(newUserName)){
            Toast.makeText(MyProfileActivity.this,"İsim alanı boş bırakılamaz",Toast.LENGTH_SHORT).show();

        } else {

            if (mainImageURI != null) {
                StorageReference imagePath = storageReference.child("Profile_Photos").child(userID + ".jpg");
                imagePath.putFile(mainImageURI).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            download_uri = task.getResult().getDownloadUrl();

                            Map<String, String> userMap = new HashMap<>();
                            userMap.put("name", userName);
                            userMap.put("image", download_uri.toString());

                            firebaseFirestore.collection("Users").document(userID).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(MyProfileActivity.this, "Profil resmi güncellendi", Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.GONE);
                                        kaydet_button.setEnabled(false);
                                        kaydet_button.setBackgroundColor(ContextCompat.getColor(MyProfileActivity.this,R.color.buttonNotEnabled));
                                    } else {
                                        String error = task.getException().getMessage();
                                        Toast.makeText(MyProfileActivity.this, "Hata! FireStore Profil resmi güncellenemedi", Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.GONE);
                                        recreate();
                                    }
                                }
                            });

                        } else {
                            String error = task.getException().getMessage();
                            Toast.makeText(MyProfileActivity.this, "Error: " + error, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
            if (NameChanged || passwordChanged) {
                if (NameChanged) {

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(newUserName)
                            .build();
                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("name update", "User name updated.");
                                    }
                                }
                            });
                }

                if (passwordChanged) {
                    final String newPass = password_new.getText().toString();
                    String newPassAgain = password_new_again.getText().toString();
                    if (TextUtils.equals(newPass, newPassAgain)) {
                        AuthCredential credential = EmailAuthProvider.getCredential(email, oldPassword);

                        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(MyProfileActivity.this, "Hata: Şifre değiştirilemedi.", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(MyProfileActivity.this, "Şifre değiştirildi.", Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        Toast.makeText(MyProfileActivity.this, "Hata: Parolalar eşleşmiyor. ", Toast.LENGTH_SHORT).show();

                    }

                }
                progressBar.setVisibility(View.GONE);
            }
            }


        //MyProfileActivity.this.recreate();
    }

    void resimSec(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(MyProfileActivity.this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                mainImageURI = result.getUri();
                profil_image.setImageURI(mainImageURI);
                //MyProfileActivity.this.recreate();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}




