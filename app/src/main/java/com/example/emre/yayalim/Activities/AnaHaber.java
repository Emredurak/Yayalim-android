package com.example.emre.yayalim.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.emre.yayalim.Model.Haber;
import com.example.emre.yayalim.Model.Model_Search;
import com.example.emre.yayalim.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;


public class AnaHaber extends AppCompatActivity {

    Haber haber =new Haber();
    ImageView haberimage;
    VideoView habervideosu;
    TextView habersahibi;
    TextView haberbasligi;
    TextView habermetni;
    TextView yorumsayisi;
    TextView begenmesayisi;
    TextView begenmemesayisi;
    TextView goruntulenmesayisi;
    ImageView yorumyap;
    ImageView begen;
    ImageView begenme;
    boolean habertipi=true;
    private int haber_id;
    private boolean begenildimi=false;
    private boolean begenilmedimi=false;

    Intent intent;

    Toolbar toolbar;
    MaterialSearchView searchView;
    Model_Search model_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anahaber_layout);

        intent = getIntent();
        haber=  intent.getExtras().getParcelable("id");

        haberimage = (ImageView) findViewById(R.id.anahaber_image);
        habervideosu = (VideoView) findViewById(R.id.anahaber_video);
        habersahibi = (TextView) findViewById(R.id.anahaber_haber_sahibi);
        haberbasligi = (TextView) findViewById(R.id.anahaber_basligi);
        habermetni = (TextView) findViewById(R.id.anahaber_icerik);
        yorumsayisi = (TextView) findViewById(R.id.anahaber_yorumsayisi);
        begenmesayisi = (TextView) findViewById(R.id.anahaber_begenmesayisi);
        begenmemesayisi = (TextView) findViewById(R.id.anahaber_begenmemesayisi);
        yorumyap = (ImageView) findViewById(R.id.anahaber_yorumlar);
        begen = (ImageView) findViewById(R.id.anahaber_begenmeler);
        begenme = (ImageView) findViewById(R.id.anahaber_begenmemeler);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_geri_renkli);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        habersahibi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                habersahibi(v);
            }
        });
        yorumyap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yorumyap(v);
            }
        });
        begen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                begen(v);

            }
        });
        begenme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                begenme(v);

            }
        });

        haberyukle();

       /* model_search = new Model_Search();
        model_search.setOnLoadSearchListener(new Model_Search.OnLoadSearchResultListener() {
            @Override
            public void onLoad(String[] sonuc) {

            }

            @Override
            public void onError() {

            }

            @Override
            public void onLocalLoad(String[] sonuc) {
                searchView.setSuggestions(sonuc);
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        search();*/
    }

    private void habersahibi(View v) {
        Intent i = new Intent(v.getContext(), Kullanici_profili.class);
        v.getContext().startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_normal,menu);

        return true;
    }

    private void haberyukle(){
        // habergorseli();
        habersahibi.setText(haber.getHaber_sahibi());
        haberbasligi.setText(haber.getHaber_basligi());
        habermetni.setText(haber.getHaber_metni());
        yorumsayisi.setText(haber.getYorum_sayisi());
        begenmesayisi.setText(haber.getBegenme_sayisi());
        begenmemesayisi.setText(haber.getBegenmeme_sayisi());

    }

    private void begen(View v) {

        if(begenildimi) {                                                       // eğer true ise yani daha önce beğenilmişse
            begen.setImageResource(R.drawable.ic_begen);         // begenme butonunun arkaplanın rengi varsayılan olur
            begenildimi=false;                                     // begenilme durumu değiştirilir.
        }
        else {                                                                  // değilse yani daha önceden beğenilmemişse
            begen.setImageResource(R.drawable.ic_begenildi);                 // begen butonu arkaplanı mavi olur.
            begenildimi=true;                                                   // begenilme durumu değişir.
            begenme.setImageResource(R.drawable.ic_begenme);                  // beğenmeme butonu da varsayılana döner.
        }
    }

    private void yorumyap(View v) {
        Intent i = new Intent(v.getContext(),Yorumlar_activity.class);
        v.getContext().startActivity(i);
    }

    private void begenme(View v) {
        if(begenilmedimi) {                                                       // eğer true ise yani daha önce işaretlenmişse
            begenme.setImageResource(R.drawable.ic_begenme);         // begenme butonunun arkaplanın rengi varsayılan olur
            begenilmedimi=false;                                     // begenilme durumu değiştirilir.
        }
        else {
            begenme.setImageResource(R.drawable.ic_begenilmedi);                 // begen butonu arkaplanı mavi olur.
            begenilmedimi=true;                                                   // begenilmeme durumu değişir.
            begen.setImageResource(R.drawable.ic_begen);                  // beğenme butonu da varsayılana döner.
        }
    }



    public void menu_goster(View view) {
            PopupMenu popup = new PopupMenu(this, view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.paylas_sikayet_menu, popup.getMenu());
            popup.show();

    }


/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.anasayfa_toolbar_menu,menu);

        MenuItem item_search = menu.findItem(R.id.search);
        searchView.setMenuItem(item_search);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search) {
            //searchView.showSuggestions();
            searchView.showSearch(true);


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }*/

   /* private void search() {
        searchView = (MaterialSearchView) findViewById(R.id.searchview);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Aranan kelime arananlar listesine eklenecek..
                // Burada arama gerçekleştirildikten sonra sonuçların yer aldığı yeni bir intent e yönlendirilecek..
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                model_search.querySearch(newText);
                return false;
            }
        });
    }*/

}


