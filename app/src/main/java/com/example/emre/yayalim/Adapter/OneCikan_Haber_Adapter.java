package com.example.emre.yayalim.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emre.yayalim.Activities.AnaHaber;
import com.example.emre.yayalim.Activities.Kullanici_profili;
import com.example.emre.yayalim.Model.Haber;
import com.example.emre.yayalim.R;

import java.util.List;

public class OneCikan_Haber_Adapter extends RecyclerView.Adapter<OneCikan_Haber_Adapter.MyViewHolder> {

    private List<Haber> haberler;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView haberbasligi, habersahibi;      /*yorumsayisi,begenmesayisi,begenmemesayisi*/
        ImageView habergorseli, sepete_ekle;
        boolean eklendimi = false;
        Haber haber;

        public MyViewHolder(View view) {
            super(view);
            habergorseli =
                    (ImageView) view.findViewById(R.id.haber_gorseli);
            haberbasligi =
                    (TextView) view.findViewById(R.id.haber_basligi);
            habersahibi =
                    (TextView) view.findViewById(R.id.haber_sahibi);

            sepete_ekle = (ImageView) view.findViewById(R.id.sepete_ekle);

            sepete_ekle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(eklendimi){
                        sepete_ekle.setImageResource(R.drawable.ic_sepete_ekle);
                        eklendimi = false;
                    }else {
                        sepete_ekle.setImageResource(R.drawable.ic_sepete_eklendi);
                        eklendimi = true;
                    }
                }
            });

            habersahibi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), Kullanici_profili.class);
                    v.getContext().startActivity(i);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //    Toast.makeText(view.getContext(),"Pos. "+pozisyon,Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(v.getContext(),AnaHaber.class);
                    i.putExtra("id", haber);
                    v.getContext().startActivity(i);
                }
            });
        }

    }


    public OneCikan_Haber_Adapter(List<Haber> haberler) {
        this.haberler = haberler;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.onecikan_haber_satir_layout, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Haber haber = haberler.get(position);
        holder.haberbasligi.setText(haber.getHaber_basligi());
        holder.habersahibi.setText(haber.getHaber_sahibi());
        /*holder.yorumsayisi.setText(haber.getYorum_sayisi());
        holder.begenmesayisi.setText(haber.getBegenme_sayisi());
        holder.begenmemesayisi.setText(haber.getBegenmeme_sayisi());*/
        holder.habergorseli.setImageResource(R.color.textColorSecondary);
        holder.haber = haber;
    }

    @Override
    public int getItemCount() {
        return haberler.size();
    }
}