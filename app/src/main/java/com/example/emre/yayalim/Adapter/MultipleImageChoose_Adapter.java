package com.example.emre.yayalim.Adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import com.example.emre.yayalim.R;

import java.util.List;

/**
 * Created by Emre on 30.6.2018.
 */

public class MultipleImageChoose_Adapter extends RecyclerView.Adapter<MultipleImageChoose_Adapter.ViewHolder> {
    public List<Uri> fileNameList;
    public List<String> fileDoneList;
    public static boolean isVideo = true;

    public MultipleImageChoose_Adapter(List<Uri> fileNameList, List<String> fileDoneList){
        this.fileDoneList = fileDoneList;
        this.fileNameList = fileNameList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.multiple_image_choose_satir_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Uri fileUri = fileNameList.get(position);

        if (isVideo) {
            try{
                holder.fileDoneView.setVisibility(View.GONE);
                holder.fileDoneVideoView.setVisibility(View.VISIBLE);
                holder.fileDoneVideoView.setVideoURI(fileUri);
                holder.fileDoneVideoView.requestFocus();
                holder.fileDoneVideoView.start();
            }catch(Exception e){
                e.printStackTrace();
            }

        } else {
            holder.fileDoneView.setVisibility(View.VISIBLE);
            holder.fileDoneVideoView.setVisibility(View.GONE);
            holder.fileDoneView.setImageURI(fileUri);
        }
    }


    @Override
    public int getItemCount() {
        return fileNameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public ImageView fileDoneView;
        public VideoView fileDoneVideoView;
        public ViewHolder(View itemView){
            super(itemView);

            mView = itemView;

            fileDoneView = mView.findViewById(R.id.secimGorseli);
            fileDoneVideoView = mView.findViewById(R.id.secimGorseliVideo);
            if(isVideo){
                fileDoneVideoView.setVisibility(View.VISIBLE);
                fileDoneView.setVisibility(View.GONE);
            }
        }
    }

}
