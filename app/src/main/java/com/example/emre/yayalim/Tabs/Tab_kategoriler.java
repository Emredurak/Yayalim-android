package com.example.emre.yayalim.Tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emre.yayalim.Adapter.Kategori_Adapter;
import com.example.emre.yayalim.Model.Kategori;
import com.example.emre.yayalim.R;

import java.util.ArrayList;
import java.util.List;

public class Tab_kategoriler extends Fragment{

    List<Kategori> kategoriler=new ArrayList<>();
    RecyclerView recyclerView_kategori;
    Kategori_Adapter adaptor;
    public Tab_kategoriler() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kategoriler_layout, container, false);

        kategoriler.add(new Kategori(1,R.drawable.hayvanlar_alemi, "Hayvanlar Alemi", "Kategori icerigi ve açıklaması"));
        kategoriler.add(new Kategori(2,R.drawable.police, "Kavga & Cinayet", "2. Kategori icerigi ve açıklaması"));
        kategoriler.add(new Kategori(3,R.drawable.insan_hayvan, "İçimizi Isıtan", "3. Kategori icerigi ve açıklaması"));
        kategoriler.add(new Kategori(4,R.drawable.trafik, "Trafik", "4. Kategori icerigi ve açıklaması"));
        kategoriler.add(new Kategori(5,R.drawable.vahsi_doga2, "Vahşi Doğa", "5. Kategori icerigi ve açıklaması"));
        kategoriler.add(new Kategori(6,R.drawable.lost_and_found, "Kayıp & Bulma", "6. Kategori icerigi ve açıklaması"));


        recyclerView_kategori = (RecyclerView) view.findViewById(R.id.recyclerview_kategoriler);
        adaptor = new Kategori_Adapter(kategoriler);
        RecyclerView.LayoutManager LayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView_kategori.setLayoutManager(LayoutManager);
        recyclerView_kategori.setItemAnimator(new DefaultItemAnimator());

        recyclerView_kategori.setAdapter(adaptor);


        return view;
    }
}