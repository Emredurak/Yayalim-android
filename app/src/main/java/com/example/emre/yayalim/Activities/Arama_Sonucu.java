package com.example.emre.yayalim.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emre.yayalim.R;

public class Arama_Sonucu extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arama_sonucu_layout);
        textView = (TextView) findViewById(R.id.deneme_text_arama);
        Intent intent = getIntent();
        doMySearch(intent.getStringExtra("query"));
       /* if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }*/
    }

    private void doMySearch(String query) {
        textView.setText(query);
        Toast.makeText(this,"Search Result",Toast.LENGTH_SHORT).show();
    }
}
