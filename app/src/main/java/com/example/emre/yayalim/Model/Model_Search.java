package com.example.emre.yayalim.Model;


public class Model_Search {
    public interface OnLoadSearchResultListener{
        void onLoad(String[] sonuc);
        void onError();
        void onLocalLoad(String [] sonuc);
    }
    private OnLoadSearchResultListener _onloadsearchlistener;
    public void setOnLoadSearchListener(OnLoadSearchResultListener l){
        this._onloadsearchlistener=l;
    }
    public void querySearch(String searchText){
        String[] liste={"Emre","Mehmet","Sami","Miko", "Yayalim","Yayalım","Yay","Yapalım"};      // deneme icin kullanıldı. Normalde sqlite üzerinden çekilecek.
        if(_onloadsearchlistener!=null){
            _onloadsearchlistener.onLocalLoad(liste);
        }

    }

}
